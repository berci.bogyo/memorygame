let dataBox = document.querySelector("[data-boxes-container]");
let cardTemplate = document.querySelector("[data-template]").innerHTML;
let row = document.getElementById('row');
let start = document.getElementById('start');
const container = document.querySelector('.card-container');
let time = document.getElementById('time');
let points = document.getElementById("yourScore");
let cardElement = [];
let flipping = 0;
let checking = [];
let point = 0;
let seconds = 0;
let minutes = 0;
console.log(points.nextElementSibling);
function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}


let arrayTwo = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13,14,14,15,15];

function makeGame(row) {
    let newCards = '';

    for (i = 0; i < 5; i++) {
        newCards += cardTemplate;
    }
    for (j = 0; j < row; j++) {
        dataBox.innerHTML += newCards;
    }
}

function setId(element, array) {
    for (i = 0; i < array.length; i++) {
        let frontCard = element[i].firstElementChild.firstElementChild.firstElementChild.children[1];
        let backCard = element[i].firstElementChild.firstElementChild.firstElementChild.children[0];
        frontCard.style.display = "none";
        backCard.setAttribute("id", i);
        frontCard.style.backgroundImage = `url(dist/images/${array[i]}.jpg)`;
    }
}

start.addEventListener('click', function () {
    makeGame(6);
    setId(container.children, shuffle(arrayTwo));
    let cards = document.querySelectorAll('.main-card');
    cards.forEach(element => {
        cardElement.push(element);
        element.addEventListener('click', function () {
            flipcard(element);

        })
    });
    start.style.display="none";
    timer();
})



function flipcard(element) {
    let one = element.firstElementChild;
    let two = one.firstElementChild;
    let three = two.firstElementChild;
    let four = three.firstElementChild;
    let image = three.children[1];
    if (flipping < 2) {
        four.style.opacity = "0";
        image.style.display="block";
        checking.push(image);
        flipping++;
        if (checking.length === 2) {
            if (checking[0].previousElementSibling.getAttribute("id") != checking[1].previousElementSibling.getAttribute("id")) {

                let firstElem = checking[0].style.backgroundImage;
                let secondElem = checking[1].style.backgroundImage;
                checkMatch(firstElem, secondElem);

                if (checkMatch(firstElem, secondElem)) {
                    let picOne = checking[0];
                    let picTwo = checking[1];
                    setTimeout(() => {
                        picOne.parentNode.parentNode.remove();
                        picTwo.parentNode.parentNode.remove();
                        point++;
                        if (point === 5) {
                            document.getElementById('yourTime').innerHTML += time.innerHTML;

                        }
                        points.nextElementSibling.innerHTML = point;
                        flipping = 0
                    }, 500)
                    flipping = 0
                } else {
                    let picOne = checking[0].previousElementSibling;
                    let picTwo = checking[1].previousElementSibling;
                    setTimeout(() => {
                        picOne.style.opacity = "1";
                        picTwo.style.opacity = "1";
                        flipping = 0
                    }, 1000)
                    flipping = 0

                }
            }
            checking = [];
        }
    }
}

function checkMatch(firstElem, secondElem) {
    if (firstElem === secondElem) {
        return true;
    } else {
        return false;
    }
}

function startTime() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
        }
    }
    time.textContent = (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
    timer();
}

function timer() {
    t = setTimeout(startTime, 1000);
}